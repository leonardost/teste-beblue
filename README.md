Teste para desenvolvedor Java backend
=====================================

Considerações
-------------

- Foi utilizada a linguagem Java 1.8 e gerenciador de dependências Maven 3.0.4.
- O projeto pode ser compilado e empacotado com o comando "mvn compile package"
- A aplicação pode ser executada pelo jar: "java -jar target/teste-beblue-1.0-SNAPSHOT.jar".
  Isso iniciará um servidor rodando em localhost na porta 8080.
- O banco H2 foi utilizado por ser mais simples.

Testes
------

Os payloads de teste foram enviados à aplicação em um sábado. Foi utilizado o comando curl da seguinte forma:

    curl -H "Content-Type: application/json" -X POST -d '{"user_cpf": 11111111111,"merchant_id": 1,"transaction_value": 100.0,"transaction_type": "TP_2"}' http://localhost:8080/register-transaction
    curl -H "Content-Type: application/json" -X POST -d '{"user_cpf": 22222222222,"merchant_id": 1,"transaction_value": 3.00,"transaction_type": "TP_3"}' http://localhost:8080/register-transaction
    curl -H "Content-Type: application/json" -X POST -d '{"user_cpf": 22222222222,"merchant_id": 1,"transaction_value": 3.00,"transaction_type": "TP_3"}' http://localhost:8080/register-transaction
    curl -H "Content-Type: application/json" -X POST -d '{"user_cpf": 33333333333,"merchant_id": 1,"transaction_value": 5.00,"transaction_type": "TP_1"}' http://localhost:8080/register-transaction

Após a execução desses comandos a saída das consultas /users e /users-transactions é, respectivamente:

    /users

    [{
        "user-id": 1,
        "user-cpf": "11111111111",
        "user-name": "João Primeiro",
        "user-balance": 47.0
    }, {
        "user-id": 2,
        "user-cpf": "22222222222",
        "user-name": "Maria Segunda",
        "user-balance": 54.12
    }, {
        "user-id": 3,
        "user-cpf": "33333333333",
        "user-name": "Emerson Terceiro",
        "user-balance": 2.25
    }, {
        "user-id": 4,
        "user-cpf": "44444444444",
        "user-name": "Mario Quarto",
        "user-balance": 89.66
    }]

    /users-transactions

    [{
        "transaction-code": "bde16f00-e366-4e89-a18b-50b4e2cad6a1",
        "transaction-date": "2017-01-21T11:49:04.765",
        "transaction-type": "TP_2",
        "transaction-value": 100.0,
        "user-cpf": "11111111111",
        "user-balance": 47.0
    }, {
        "transaction-code": "beb51231-cba7-4495-8ef2-a96443a92adc",
        "transaction-date": "2017-01-21T11:49:28.296",
        "transaction-type": "TP_3",
        "transaction-value": 3.0,
        "user-cpf": "22222222222",
        "user-balance": 53.07
    }, {
        "transaction-code": "e959c2e6-50b2-4295-bd50-52a68b9e1e9e",
        "transaction-date": "2017-01-21T11:49:31.310",
        "transaction-type": "TP_3",
        "transaction-value": 3.0,
        "user-cpf": "22222222222",
        "user-balance": 54.12
    }]

Existem apenas 3 transações porque o teste 4 retorna uma resposta de erro,
já que o saldo do usuário é insuficiente para processar a transação.
