package br.com.testebeblue.app;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import org.hibernate.cfg.Configuration;

import org.hibernate.criterion.Restrictions;

import br.com.testebeblue.app.model.Merchant;
import br.com.testebeblue.app.model.MoneyTransaction;
import br.com.testebeblue.app.model.TransactionType;
import br.com.testebeblue.app.model.User;

@RestController
public class AplicacaoController {

    private SessionFactory sessionFactory;
    private Map<String, String> transactionTypes;

    public AplicacaoController() {
        sessionFactory = new Configuration().configure().buildSessionFactory();
        loadUsers();
        loadMerchant();
        loadTransactionTypes();
    }

    private void loadUsers() {
        RestTemplate restTemplate = new RestTemplate();
        User[] resposta = restTemplate.getForObject("https://quarkbackend.com/getfile/vilibaldo-neto/json-javatest-users", User[].class);
        List<User> users = Arrays.asList(resposta);

        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        for (User u : users) {
            Query query = session.createQuery("select 1 from User t where t.cpf = :cpf");
            query.setString("cpf", u.getCpf());
            if (query.uniqueResult() == null) {
                transaction = session.beginTransaction();
                session.save(u);
                transaction.commit();
            }
        }

        session.close();
    }

    private void loadMerchant() {
        Merchant merchant = new Merchant(
                1L,
                "Estabelecimento de teste",
                0.05, 0.10, 0.15, 0.20, 0.25, 0.30, 0.35
        );

        Session session = sessionFactory.openSession();

        Query query = session.createQuery("select 1 from Merchant t where t.id = :id");
        query.setLong("id", 1L);
        if (query.uniqueResult() != null) {
            session.close();
            return;
        }

        Transaction transaction = session.beginTransaction();
        session.save(merchant);
        transaction.commit();
        session.close();
    }

    private void loadTransactionTypes() {
        RestTemplate restTemplate = new RestTemplate();
        transactionTypes = new HashMap<String, String>();
        TransactionType[] resposta = restTemplate.getForObject("https://quarkbackend.com/getfile/vilibaldo-neto/json-javatest-transactiontypr", TransactionType[].class);
        for (TransactionType t : resposta) {
            transactionTypes.put(t.getType(), t.getName());
        }
    }

    /**
     * Faz o processamento e registro de uma transação monetária.
     */
    @RequestMapping(value = "/register-transaction", method = RequestMethod.POST)
    public Response registerTransaction(@RequestBody MoneyTransaction moneyTransaction) {

        User user = recoverUser(moneyTransaction.getUserCpf());

        if (!userHasEnoughForCashback(moneyTransaction, user)) {
            return new Response(500, "ERRO - Saldo insuficiente");
        }

        processTransaction(moneyTransaction, user);

        return Response.ok();
    }

    private User recoverUser(String cpf) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(User.class)
                .add(Restrictions.eq("cpf", cpf));
        User user = (User) criteria.uniqueResult();
        session.close();
        return user;
    }

    private Merchant recoverMerchant(Long id) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Merchant.class)
                .add(Restrictions.eq("id", id));
        Merchant merchant = (Merchant) criteria.uniqueResult();
        session.close();
        return merchant;
    }

    private boolean userHasEnoughForCashback(MoneyTransaction moneyTransaction, User user) {
        String transactionName = transactionTypes.get(moneyTransaction.getType());
        if (transactionName.equals("CASHBACK") && user.getBalance() < moneyTransaction.getValue()) {
            return false;
        }
        return true;
    }

    private void processTransaction(MoneyTransaction moneyTransaction, User user) {

        Merchant merchant = recoverMerchant(moneyTransaction.getMerchantId());
        String transactionName = transactionTypes.get(moneyTransaction.getType());

        if (transactionName.equals("CASHBACK")) {
            user.spend(moneyTransaction.getValue());
            moneyTransaction.setCashback(-moneyTransaction.getValue());
        } else {
            // Usuário é créditado de acordo com o cashback do estabelecimento
            int dayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
            double cashbackPercentage = merchant.getCashbackPercentage(dayOfWeek);
            double cashback = moneyTransaction.getValue() * cashbackPercentage;
            user.credit(cashback);
            moneyTransaction.setCashback(cashback);
        }

        moneyTransaction.setBalance(user.getBalance());

        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.saveOrUpdate(user);
        session.save(moneyTransaction);
        tx.commit();
        session.close();
    }

    /**
     * Retorna lista de usuários
     */
    @RequestMapping("/users")
    public List<User> users() {
        Session session = sessionFactory.openSession();
        List<User> users = session.createCriteria(User.class).list();
        session.close();
        return users;
    }

    /**
     * Retorna lista de transações
     */
    @RequestMapping("/users-transactions")
    public List<MoneyTransaction> usersTransactions() {
        Session session = sessionFactory.openSession();
        List<MoneyTransaction> transactions = session.createCriteria(MoneyTransaction.class).list();
        session.close();
        return transactions;
    }

}
