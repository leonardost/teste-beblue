package br.com.testebeblue.app;

public class Response {

    private Integer code;
    private String message;

    public Response(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public static Response ok() {
        return new Response(200, "OK");
    }

}
