package br.com.testebeblue.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "`user`")
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String cpf;
    private String name;
    private Double balance;

    public User() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("user-id")
    public Long getId() {
        return this.id;
    }

    @JsonProperty("user_cpf")
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @JsonProperty("user-cpf")
    public String getCpf() {
        return this.cpf;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("user-name")
    public String getName() {
        return this.name;
    }

    @JsonProperty("balance")
    public void setBalance(Double balance) {
        this.balance = balance;
    }

    @JsonProperty("user-balance")
    public Double getBalance() {
        return this.balance;
    }

    public void credit(Double value) {
        this.balance += value;
    }

    public void spend(Double value) {
        this.balance -= value;
    }

    @Override
    public String toString() {
        return "Usuario '" + name + "' (CPF " + cpf + ") tem saldo R$" + balance;
    }

}
