package br.com.testebeblue.app.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TransactionType {

    @JsonProperty("transaction_type")
    private String type;
    @JsonProperty("transaction_name")
    private String name;

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Tipo de transação: " + type + "-" + name;
    }

}
