package br.com.testebeblue.app.model;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Merchant {

    @Id
    private Long id;
    private String name;
    private Double cashbackSunday;
    private Double cashbackMonday;
    private Double cashbackTuesday;
    private Double cashbackWednesday;
    private Double cashbackThursday;
    private Double cashbackFriday;
    private Double cashbackSaturday;

    public Merchant() {
    }

    public Merchant(Long id, String name, Double c1, Double c2, Double c3, Double c4, Double c5, Double c6, Double c7) {
        this.id = id;
        this.name = name;
        this.cashbackSunday = c1;
        this.cashbackMonday = c2;
        this.cashbackTuesday = c3;
        this.cashbackWednesday = c4;
        this.cashbackThursday = c5;
        this.cashbackFriday = c6;
        this.cashbackSaturday = c7;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setCashbackSunday(Double cashbackMonday) {
        this.cashbackSunday = cashbackSunday;
    }

    public Double getCashbackSunday() {
        return cashbackSunday;
    }

    public void setCashbackMonday(Double cashbackMonday) {
        this.cashbackMonday = cashbackMonday;
    }

    public Double getCashbackMonday() {
        return cashbackMonday;
    }

    public void setCashbackTuesday(Double cashbackTuesday) {
        this.cashbackTuesday = cashbackTuesday;
    }

    public Double getCashbackTuesday() {
        return cashbackTuesday;
    }

    public void setCashbackWednesday(Double cashbackWednesday) {
        this.cashbackWednesday = cashbackWednesday;
    }

    public Double getCashbackWednesday() {
        return cashbackWednesday;
    }

    public void setCashbackThursday(Double cashbackThursday) {
        this.cashbackThursday = cashbackThursday;
    }

    public Double getCashbackThursday() {
        return cashbackThursday;
    }

    public void setCashbackFriday(Double cashbackFriday) {
        this.cashbackFriday = cashbackFriday;
    }

    public Double getCashbackFriday() {
        return cashbackFriday;
    }

    public void setCashbackSaturday(Double cashbackSaturday) {
        this.cashbackSaturday = cashbackSaturday;
    }

    public Double getCashbackSaturday() {
        return cashbackSaturday;
    }

    public Double getCashbackPercentage(int day) {
        switch (day) {
            case Calendar.SUNDAY:
                return cashbackSunday;
            case Calendar.MONDAY:
                return cashbackMonday;
            case Calendar.TUESDAY:
                return cashbackTuesday;
            case Calendar.WEDNESDAY:
                return cashbackWednesday;
            case Calendar.THURSDAY:
                return cashbackThursday;
            case Calendar.FRIDAY:
                return cashbackFriday;
            case Calendar.SATURDAY:
                return cashbackSaturday;
        }
        return null;
    }

}
