package br.com.testebeblue.app.model;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class MoneyTransaction {

    @Id
    @JsonProperty("transaction-code")
    private String code;
    @JsonProperty("transaction-date")
    private LocalDateTime date;
    private String type;
    private Double value;
    private String userCpf;
    @JsonProperty("user-balance")
    private Double balance;
    @JsonIgnore
    private Double cashback;
    private Long merchantId;

    public MoneyTransaction() {
        this.code = UUID.randomUUID().toString();
        this.date = LocalDateTime.now();
    }

    public MoneyTransaction(User user, Merchant merchant, Double value, String type) {
        this.code = UUID.randomUUID().toString();
        this.date = LocalDateTime.now();
        this.userCpf = user.getCpf();
        this.merchantId = merchant.getId();
        this.balance = value;
        this.type = type;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getDate() {
        return date.toString();
    }

    @JsonProperty("transaction_type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("transaction-type")
    public String getType() {
        return type;
    }

    @JsonProperty("user_cpf")
    public void setUserCpf(String userCpf) {
        this.userCpf = userCpf;
    }

    @JsonProperty("user-cpf")
    public String getUserCpf() {
        return userCpf;
    }

    @JsonProperty("merchant_id")
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    @JsonIgnore
    public Long getMerchantId() {
        return merchantId;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Double getBalance() {
        return balance;
    }

    @JsonProperty("transaction_value")
    public void setValue(Double value) {
        this.value = value;
    }

    @JsonProperty("transaction-value")
    public Double getValue() {
        return value;
    }

    public void setCashback(Double cashback) {
        this.cashback = cashback;
    }

    public Double getCashback() {
        return cashback;
    }

}
